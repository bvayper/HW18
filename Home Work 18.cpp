#include <iostream>
#include <string>
#include <stack>
#include <iomanip>

template <typename T>
class Stack
{
private:
	int size;
	int top;
	T* stackPtr;
public:
	Stack(int = 10); // Stack max size
	Stack(const Stack<T>&);
	void push(const T&);
	T pop();
	void printStack();
	~Stack();
}; // Stack class

template <typename T>
Stack<T>::Stack(int MaxSize) : size(MaxSize)
{
	stackPtr = new T[size];
	top = 0;
} //Stack size

template <typename T>
void Stack<T>::push(const T& value)
{
	stackPtr[top++] = value;
} // push function

template <typename T>
T Stack<T>::pop()
{
	return stackPtr[--top];
	stackPtr[--top];
} //pop function

template <typename T>
Stack<T>::~Stack()
{
	delete[] stackPtr;
} // delete function (for new/delete)

template <typename T>
void Stack<T>::printStack()
{
    for (int i = top - 1; i >= 0; i--)
        std::cout<< stackPtr[i] << '\n';
}

int main()
{
	Stack<int> stackSymbol(6);
	int ct = 0;
	int nt;
	std::cout << "gimme 6 integer numbers : "<<'\n';
	while (ct++ < 6)
	{
		std::cin >> nt;
		stackSymbol.push(nt);
	} // 6 int stack
	std::cout << '\n';
	stackSymbol.printStack();
	std::cout << '\n' <<"delete " <<stackSymbol.pop() << '\n';
	std::cout << "delete " <<stackSymbol.pop() << '\n' <<"also delete two integer";
	stackSymbol.pop();
	stackSymbol.pop();
	std::cout << '\n' << "whats left : " <<'\n';
	stackSymbol.printStack();

	Stack<std::string> stackString(4);
	int cot = 0;
	std::string st;
	std::cout << "gimme 4 string : " << '\n';
	while (cot++ < 4)
	{
		std::cin >> st;
		stackString.push(st);

	} // 4 string stack
	std::cout << '\n';
	stackString.printStack();
	std::cout << '\n'<<"delete one string";
	stackString.pop();
	std::cout <<'\n' << "then delete second one :" << '\n' << stackString.pop() << '\n' << "what left :" <<'\n';
	stackString.printStack();

	return 0;
}